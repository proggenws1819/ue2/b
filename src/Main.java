/**
 * Main Klasse die die Methode getObject() der Klasse Singleton aufruft
 *
 * @author Cedrico Knoesel
 * @since 05.11.18
 */
public class Main {

    public static void main(String[] args) {
        Singleton.getObject();
    }
}
