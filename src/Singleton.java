import java.util.Calendar;

/**
 * Klasse die nach dem Singleton Prinzip ein selbstreferenzierendes Klassenattribut besitzt und über die Methode
 * getObject() dieses entweder erzeugt und zurückgibt, oder nur zurückgibt.
 *
 * @author Cedrico Knoesel
 * @since 05.12.2018
 */
public class Singleton {
    private static Singleton object;

    private Singleton() {
        Calendar calendar = Calendar.getInstance();
        System.out.printf("Aktuelles Datum: %td.%tm.%tY ", calendar, calendar, calendar);
    }

    /**
     * gibt Instanz der Singleton Klasse zurück und erzeugt sie zuvor, falls noch nicht geschehen
     *
     * @return Singleton Instanz
     */
    public static Singleton getObject() {
        if (object == null) {
            object = new Singleton();
        }
        return object;
    }
}